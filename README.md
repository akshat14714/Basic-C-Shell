# Basic Shell Impementation Using C

## Authors:

	Akshat Maheshwari (20161024)
	Samyak Jain (20161083)

## Building and Implementation:
	First run the command *make* in the project directory.
	The run *./shell* to run the shell and start interacting with it.

## Features:
- All the 5 basic tasks given have been implemented.
- SemiColon separated commands accepted, but while using this, you have to give spaces between commands and semi-colons.
- Modularity: All the different commands' functions have been defined in different files.
- One single header file **shell.h** containing all the global variables.
- We have defined a **Makefile** that has all the operations of compiling all the files.
- Ctrl-C cannot exit the shell.
- exit command for quitting the shell.
